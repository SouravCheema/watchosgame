//
//  LevelDifficulty.swift
//  watchOSTest WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-14.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit
import Foundation


class LevelDifficulty: WKInterfaceController {
    
    
    let defaults = UserDefaults.standard
    @IBOutlet weak var EasySelected: WKInterfaceButton!
    
    @IBOutlet weak var HardButton: WKInterfaceButton!
    
    var userSelectedEasy: Bool = true
    var userSelectedHard: Bool = false
    var levelDifficulty: String = ""
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    @IBAction func EasyButtonPressed() {
        
        if(userSelectedEasy) {
            self.levelDifficulty = "Easy"
            
            defaults.set(self.levelDifficulty, forKey: "Difficulty")
            presentController(withName: "InterfaceController", context: nil)
        }
        
    }
    
    @IBAction func HardButtonPreseed() {
        self.userSelectedEasy = false
        self.userSelectedHard = true
        if(userSelectedHard) {
            
            self.levelDifficulty = "Hard"
            
            defaults.set(self.levelDifficulty, forKey: "Difficulty")
            presentController(withName: "InterfaceController", context: nil)
        }
    }
    
    
}
