//
//  InterfaceController.swift
//  watchOSTest WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-14.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit
import Foundation
import SpriteKit

class InterfaceController: WKInterfaceController {

    
    var arr : [UIImage] = []
    
    var gameLevel = UserDefaults.standard.value(forKey: "Difficulty") as? String
    var timer:Timer?
    
    var image1: UIImage!
    var image2: UIImage!
    var image3: UIImage!
    var image4: UIImage!
    
    

    
    @IBOutlet weak var MemorizeImage1: WKInterfaceImage!
    
    @IBOutlet weak var MemorizeImage2: WKInterfaceImage!
    
    
    @IBOutlet weak var MemorizeImage3: WKInterfaceImage!
    
    
    @IBOutlet weak var MemorizeImage4: WKInterfaceImage!
    
    
    @IBOutlet weak var selectImage1: WKInterfaceButton!
    
    @IBOutlet weak var selectImage2: WKInterfaceButton!
    
    @IBOutlet weak var selectImage3: WKInterfaceButton!
    
    @IBOutlet weak var selectImage4: WKInterfaceButton!
    
    
    let defaults = UserDefaults.standard
    @IBOutlet weak var gamerTagLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if(self.gameLevel == "Easy") {
            self.easySelected()
        }

        if(self.gameLevel == "Hard") {
            self.hardSelected()
        }
        
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func userNameButton() {
        self.getUserName()
    }
    
    
    
    
    
    @IBAction func startGameButton() {
    }
    
    func getUserName() {
        let suggestionsList = ["Pumpkin", "Carrot", "EggPlant"]
        
        presentTextInputController(withSuggestions: suggestionsList, allowedInputMode: .plain) {
            
            (results) in
            // Put your code here
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.gamerTagLabel.setText("Hello " + userResponse! + "!")
                self.defaults.set(userResponse, forKey: "userResponse")
                
                
            }

        }
    }
    
    

    
    
    func assignImages() {
//        self.MemorizeImage1 = UIImage(named: "001-guitar")
//        self.MemorizeImage2 = UIImage(named: "002-soccer-ball-variant")
//        self.MemorizeImage3 = UIImage(named: "003-tomato")
//        self.MemorizeImage4 = UIImage(named: "004-bird")
        
        self.MemorizeImage1?.setImage(UIImage(named: "001-guitar"))
        self.MemorizeImage2?.setImage(UIImage(named: "002-soccer-ball-variant"))
        self.MemorizeImage3?.setImage(UIImage(named: "003-tomato"))
        self.MemorizeImage4?.setImage(UIImage(named: "004-bird"))
    }
    
    
    func easySelected() {
//        self.gameLevel = UserDefaults.standard.value(forKey: "Difficluty") as! String
        
            self.assignImages()
            
            Timer.scheduledTimer(withTimeInterval: 7, repeats: false) { (Timer) in
                self.MemorizeImage1?.setHidden(true)
                self.MemorizeImage2?.setHidden(true)
                self.MemorizeImage3?.setHidden(true)
                self.MemorizeImage4?.setHidden(true)
                
                self.selectImage1?.setTitle("")
                self.selectImage2?.setTitle("")
                self.selectImage3?.setTitle("")
                self.selectImage4?.setTitle("")
                
                self.selectImage1?.setBackgroundImage(UIImage(named: "001-guitar"))
                self.selectImage2?.setBackgroundImage(UIImage(named: "002-soccer-ball-variant"))
                self.selectImage3?.setBackgroundImage(UIImage(named: "003-tomato"))
                self.selectImage4?.setBackgroundImage(UIImage(named: "004-bird"))
                
                self.image1 =  UIImage(named: "001-guitar")
                self.image2 =  UIImage(named: "002-soccer-ball-variant")
                self.image3 =  UIImage(named: "003-tomato")
                self.image4 =  UIImage(named: "004-bird")
            }
            
        
    }
    
    func hardSelected(){
        
            self.assignImages()
            
            Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (Timer) in
                self.MemorizeImage1?.setHidden(true)
                self.MemorizeImage2?.setHidden(true)
                self.MemorizeImage3?.setHidden(true)
                self.MemorizeImage4?.setHidden(true)
                
                self.selectImage1?.setTitle("")
                self.selectImage2?.setTitle("")
                self.selectImage3?.setTitle("")
                self.selectImage4?.setTitle("")
                
                self.selectImage1?.setBackgroundImage(UIImage(named: "001-guitar"))
                self.selectImage2?.setBackgroundImage(UIImage(named: "002-soccer-ball-variant"))
                self.selectImage3?.setBackgroundImage(UIImage(named: "003-tomato"))
                self.selectImage4?.setBackgroundImage(UIImage(named: "004-bird"))
                
                self.image1 =  UIImage(named: "001-guitar")
                self.image2 =  UIImage(named: "002-soccer-ball-variant")
                self.image3 =  UIImage(named: "003-tomato")
                self.image4 =  UIImage(named: "004-bird")
                
            }
        }
    
    @IBAction func firstButtonPressed() {
        
        self.MemorizeImage1?.setImage(UIImage(named: "\(self.image1)"))
        self.MemorizeImage1.setHidden(false)
    }
    
    @IBAction func secondButtonPressed() {
        
        self.MemorizeImage2?.setImage(UIImage(named: "\(self.image2)"))
        self.MemorizeImage2.setHidden(false)
    }
    
    @IBAction func thirdButtonPressed() {
        
        self.MemorizeImage3?.setImage(UIImage(named: "\(self.image3)"))
        self.MemorizeImage3.setHidden(false)
    }
    
    @IBAction func fourthButtonPressed() {
        
        self.MemorizeImage4?.setImage(UIImage(named: "\(self.image4)"))
        self.MemorizeImage4.setHidden(false)
    }
    }


